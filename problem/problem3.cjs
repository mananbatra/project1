let dataSet=require('../dataSet.cjs')
const sortData=(dataSet)=>
{
    function compare(a,b)
    {
      if(a.car_model.toLowerCase()<b.car_model.toLowerCase())
      {
        return -1;
      }
      if(a.car_model.toLowerCase()>b.car_model.toLowerCase())
      {
        return 1;
      }
      return 0;
    }
        
      dataSet.sort(compare);
      //console.log(dataSet);
}

sortData(dataSet);

module.exports = dataSet;
